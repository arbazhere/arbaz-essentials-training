/**
 *@NApiVersion 2.x
 *@NScriptType UserEventScript
 */

define(['N/https','N/ui/dialog','N/url','N/currentRecord'],

    function(https,dialog,url,currentRecord) {

        var entityRecord;

        function beforeLoad(context){
            //console.log(context.mode);
            var currentRecObj= context.newRecord;
            entityRecord = currentRecObj.id;
            log.debug({title: 'Init Name', details: entityRecord});
            var form = context.form;
            var suiteletURL = url.resolveScript({
                scriptId:'customscript_eval_2', //'customscript_eval_script',
                deploymentId:'customdeploy_eval_2', //'customdeploy_sr_sut_emailinvoice',
                returnExternalUrl: false,
                // params: {
                //     'name': name,
                // }
            });
            form.addButton({
                id: 'custpage_create_inv_adj',
                label: 'Pay',
                functionName: 'var link = window.location.origin + "' + suiteletURL +
                '&name=' + entityRecord + '";' +
                'window.open(link, "_blank")'
            });
            
            //context.form.addButton('test_button','test');
            
        };
        // function validateField(context) {
        //     if (context.mode !== 'edit')
        //         return;
        //     var currentRecord = context.currentRecord;
        //     var entityRecord = currentRecord.getValue({fieldId: 'companyname'});
        // }
        return {
            beforeLoad: beforeLoad
            //validateField: validateField
        };

});