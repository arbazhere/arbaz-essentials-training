/**
 *@NApiVersion 2.0
 *@NScriptType ClientScript
 */

define(['N/search'],

    function(search) {

        function savedSearch() {
            var mySalesOrderSearch = search.create({
                type: search.Type.SALES_ORDER,
                title: 'My SalesOrder Search',
                id: 'customsearch_arbaz_so',
                columns: [{
                    name: 'entity'
                }, {
                    name: 'subsidiary'
                }, {
                    name: 'name'
                }, {
                    name: 'currency'
                }],
                filters: [{
                    name: 'mainline',
                    operator: 'is',
                    values: ['T']
                }],
                settings: [{
                    name: 'consolidationtype',
                    value: 'AVERAGE'
                }]
            });
            mySalesOrderSearch.save();
        }
        return {
            pageInit: savedSearch
        };

});
