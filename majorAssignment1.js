/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 */

define(['N/ui/serverWidget','N/record', 'N/search','N/task'], function(serverWidget,record,search,task) {
    function onRequest(context) {
        if (context.request.method === 'GET') {

            var form = serverWidget.createForm({
                title: 'Major Assignment'
            });

            var fromgroup = form.addFieldGroup({
                id: 'fromgroup',
                label: 'From Dates'
            });
            fromgroup.isSingleColumn = true;

            var togroup = form.addFieldGroup({
                id: 'togroup',
                label: 'To Dates'
            });
            togroup.isSingleColumn = true;
            
            var fromDatePicker = form.addField({
                id: 'fromdate',
                type: serverWidget.FieldType.DATE,
                label: 'Select Date',
                container: 'fromgroup'
            });
            fromDatePicker.updateLayoutType({
                    layoutType: serverWidget.FieldLayoutType.STARTROW
                });

            var toDatePicker = form.addField({
                id: 'todate',
                type: serverWidget.FieldType.DATE,
                label: 'Select Date',
                container: 'togroup'
            });
            toDatePicker.updateLayoutType({
                    layoutType: serverWidget.FieldLayoutType.STARTROW
                });

            form.addSubmitButton({
                label: 'Submit'
            });

            context.response.writePage(form);
            
        }

        else if(context.request.method === 'POST' && context.request.parameters.todate && context.request.parameters.fromdate){
            var form = serverWidget.createForm({
                title: 'Form 2'
            });
            
            var sublist = form.addSublist({
                id : 'sublist',
                type : serverWidget.SublistType.LIST,
                label : 'Sales Order List'
            })
            sublist.displayType = serverWidget.SublistDisplayType.NORMAL;
            

            var mySalesOrderSearch = search.create({
                type: search.Type.SALES_ORDER,
                title: 'My Search',
                id: 'customsearch_major_assignment',
                columns: [{
                    name: 'entity',}
                , {
                    name: 'tranId'}
                , {
                    name: 'internalid'}
                , {
                    name: 'status'}
                , {
                    name: 'recordType'
                }],
                filters: [['trandate','onorafter',context.request.parameters.fromdate], 'and' ,['trandate','before', context.request.parameters.todate], 'and' , ['mainline','is','T']]
            });
            var searchResults = mySalesOrderSearch.run();

            
            var check = sublist.addField({
                    id : 'checkbox',
                    label : 'CheckBox',
                    type : serverWidget.FieldType.CHECKBOX
                });

            check.updateDisplayType({displayType: serverWidget.FieldDisplayType.ENTRY});

            sublist.addField({
                id : 'custpage_item',
                label : 'Order Id',
                type : serverWidget.FieldType.TEXT
            });

            var stat = sublist.addField({
                id : 'status',
                label : 'Status',
                type : serverWidget.FieldType.TEXT
            });
            
            sublist.addField({id: 'internal',type: serverWidget.FieldType.TEXT, label: 'Internal ID'}).updateDisplayType({displayType: serverWidget.FieldDisplayType.HIDDEN});
            sublist.addField({id: 'recordtype',type: serverWidget.FieldType.TEXT, label: 'Record Type'}).updateDisplayType({displayType: serverWidget.FieldDisplayType.HIDDEN});
            
            var sublist = form.getSublist({
                id : 'sublist'
            });
            var i = 0;
            var count = 0;
            searchResults.each(function(result) {
                
                //var transId = result.id;
                var entityId = result.getValue('internalid'); 
                //var entityName = result.getText('tranId');
                var status = result.getValue('status');
                if(status =='pendingFulfillment'){
                    count = count + 1;
                    sublist.setSublistValue({
                        id : 'custpage_item',
                        line : i,
                        value : entityId
                        });
                    sublist.setSublistValue({
                        id : 'status',
                        line : i,
                        value : status
                        });
                    sublist.setSublistValue({
                        id: 'recordtype',
                        line: i,
                        value: 'pending'
                    })
                    i= i+1;
                }
                
                return true;
            });
            
            form.addSubmitButton({
                label: 'Submit'
            });
            context.response.writePage(form);
            
        }
        else{
            var count = context.request.getLineCount('sublist');
            log.debug({
                title: 'testing',
                details: 'Total ' + count
            })
            var custRecord = record.create({
                type: 'customrecord_pending_fulfillment_so'
            }) ;
            for(var i=0; i< count; i++){
                var check = context.request.getSublistValue('sublist', 'checkbox', i);
                log.debug({
                    title: 'check',
                    details: check
                })
                
                if(check != 'F')
                {
                    var internalId = context.request.getSublistValue({
                        group: 'sublist',
                        name: 'custpage_item',
                        line: i
                    });
                    var record_type = context.request.getSublistValue({
                        group: 'sublist', 
                        name:'recordtype', 
                        line: i
                    });
                    log.debug({
                        title: 'Id',
                        details: internalId + ' ' + record_type
                    })
                    custRecord.setValue(
                        {
                            fieldId: 'custrecord_sales_order',
                            value: internalId
                        })
                    custRecord.setValue({
                        fieldId: 'custrecord1397',
                        value: record_type
                    })
                }
                custRecord.save();
                
            
            }
            
            
            //var mySalesRecord = context.newRecord;
            var scriptTask = task.create({ taskType: task.TaskType.SCHEDULED_SCRIPT }); 
            scriptTask.scriptId = 'customscript_ma_day9_2';//enter your scheduled script id here;
            scriptTask.deploymentId = 'customdeploy_deploy_day9' ;//enter scheduled script deployment id here;
            var scheduledScriptTaskId = scriptTask.submit();
        }
    }
    return {
        onRequest: onRequest
    };
});
