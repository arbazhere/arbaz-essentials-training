/**
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 */
define(['N/https'],function(https){
    function getInputData() {
        var apiResponse = https.get({
            url: 'https://0648c6f77e5cbdb0fd0bfff018c42578:9b497a1040bdc348b35bc40c22d1365a@new-folio3-test-store.myshopify.com/admin/orders.json'
        });
        log.debug({title: "api", details: apiResponse.body});
        var data = JSON.parse(apiResponse.body);
        return data.orders;
    }
    function map(context) {
        var customerSearch = search.create({
            type: search.Type.CUSTOMER,
            title: 'Customer Search',
            id: 'customsearch_customer_day10',
            columns: [{
                name: 'entityid',}
            , {
                name: 'email'}],
            filters: [['email','is',context.value['email']]]
        });
        log.audit({title:'email',details:context.value});
        var searchResults = customerSearch.run();
        var itemSearch = search.create({
            type: search.Type.ITEM,
            title: 'Item Search',
            id: 'customsearch_item_day10',
            columns: [{
                name: 'itemid'
            }],
            filters: [['itemid','is',context.value.line_items.sku]]
        })
        var itemResults = itemSearch.run();
        log.debug({
            title:'items',
            details: itemResults});
        log.debug({
            title:'Customer',
            details: searchResults
        });
        if(itemResults.length > 0)
        {
            if(searchResults.length > 0)
            {
                context.write({
                    key: searchResults,
                    value: itemResults
                })
            }
            else
            {
                cust = record.create(
                    {
                        type: record.Type.CUSTOMER 
                    }
                );
                cust.setValue({
                    fieldId: 'email',
                    value: searchResults
                });
                searchResults = context.value.email;
                context.write({
                    key: searchResults,
                    value: itemResults
                })

            }

        }
    }


    function reduce(context){

        var salesOrder = record.create({
            type: record.Type.SALES_ORDER
        });
        salesOrder.setValue({
            fieldId: 'email',
            value: context.key
        });
        salesOrderselectNewLine({
            sublistId: 'item'
        });
        salesOrder.setCurrentSublistValue({
            sublistId: 'item',
            fieldId: 'item',
            value: context.value
        });

        context.write({
            key: salesOrder.getValue({
                fieldId: 'email' 
            }),
            value: salesOrder.getValue({
                fieldId: 'item'
            })
        });

    }
    return{
        getInputData: getInputData,
        map: map,
        reduce: reduce
    }
    }
);