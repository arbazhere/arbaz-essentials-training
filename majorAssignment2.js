/**
 *@NApiVersion 2.1
 *@NScriptType ScheduledScript
 */

define(['N/record'], 
    function(record){
        function execute(context) {
            var currentRecord = record.load({
                type: 'customrecord_pending_fulfillment_so',
                id: 479
            });

            var transformRecord = record.transform({
                fromType: 'customrecord_pending_fulfillment_so',
                fromId: 479,
                toType: record.Type.ITEM_FULFILLMENT
            });
            currentRecord.setValue({
                fieldId: 'custrecord1397',
                value: 'fulfilled'
            })
            
            currentRecord.save();
        }
        return {
            execute: execute
        };
    }
);