/**
 *@NApiVersion 2.1
 *@NScriptType ClientScript
 */

define([], 
    function(){
        function fieldChanged(context) {
            var currentRecord = context.currentRecord;
            var fieldName = context.fieldId;
            if (fieldName == "location")
            {
                currentRecord.setValue({
                    fieldId: 'location',
                    value: 'US - West Coast Warehouse'
                });
                console.log("Testing day 4 script");
            }  
        }
        return {
            fieldChanged: fieldChanged
        };
    }
);