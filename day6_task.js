/**
 *@NApiVersion 2.x
 *@NScriptType UserEventScript
 */

// Load two standard modules.
define ( ['N/record', 'N/ui/serverWidget'] ,
    
    function(record, serverWidget) {

        function afterSubmit(context){
            var newSalesRecord = context.newRecord;
            var newPaymentMethod = newSalesRecord.getValue({
                fieldId: 'paymentmethod'
            });
            
            if(newPaymentMethod){
                
                var objRecord = record.transform({
                    fromType: record.Type.SALES_ORDER,
                    fromId: newSalesRecord.id,
                    toType: record.Type.CASH_SALE
                });
            }
            else{
                var objRecord = record.transform({
                    fromType: record.Type.SALES_ORDER,
                    fromId: newSalesRecord.id,
                    toType: record.Type.INVOICE
                });
            }
            try {
                var newTaskId = objRecord.save();
                log.debug({
                    title: 'Task record created successfully',
                    details: 'New task record ID:  ' + newTaskId
                });
            } catch (e) {
                log.error({
                    title: e.name,
                        details: e.message
                });
            }
            

        }
        return {
            
            afterSubmit: afterSubmit
        };

    });