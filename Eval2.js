/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 */
define(['N/ui/serverWidget','N/search','N/record','N/email'], 
function(serverWidget,search,record,email) {
    function onRequest(context) {
        if (context.request.method === 'GET') {
            var name = context.request.parameters.name;
            var form = serverWidget.createForm({
                title: "Sales Order Search"
            });


            var saleOrder = form.addSublist({
                id : 'salesorderslists',
                type : serverWidget.SublistType.LIST,
                label : 'Sales Orders'
            });
            saleOrder.addField({
                id: 'sublist_check',
                type: serverWidget.FieldType.CHECKBOX,
                label: 'Checkbox'
            });
            saleOrder.addField({
                id: 'sublist_id',
                type: serverWidget.FieldType.TEXT,
                label: 'Order ID'
            });

            
            saleOrder.addField({
                id: 'sublist_customer',
                type: serverWidget.FieldType.TEXT,
                label: 'Customer.'
            });

            saleOrder.addField({
                id: 'sublist_status',
                type: serverWidget.FieldType.TEXT,
                label: 'Status'
            });
            // var customerRecord = record.load({
            //     type: record.Type.CUSTOMER,
            //     id: entity
            // });
            var mySalesOrderSearch = search.create({
                type: search.Type.SALES_ORDER,
                filters: [

                    ['mainline','is', 'T']
                ],
                columns: ['status','entity','tranid','trandate']

            })
            var res = mySalesOrderSearch.run();

            var sublist = form.getSublist({
                id : 'salesorderslists'
            });
            var i = 0;
            var count = 0;
            res.each(function(result) {
                
                //var transId = result.id;
                var tranId = result.getValue('tranid'); 
                //var entityName = result.getText('tranId');
                var status = result.getValue('status');
                var entity = result.getValue('entity');

                if(status =='pendingBilling' && entity == name){
                    count = count + 1;
                    sublist.setSublistValue({
                        id : 'sublist_id',
                        line : i,
                        value : result.id
                        });
                    sublist.setSublistValue({
                        id : 'sublist_status',
                        line : i,
                        value : status
                        });
                    sublist.setSublistValue({
                        id: 'sublist_customer',
                        line: i,
                        value: entity
                    });
                    i= i+1;
                }
                
                return true;
            });

            form.addSubmitButton({
                label: 'Submit'
            });
            //var entityid = context.request.parameters.entityRecord;
            context.response.writePage(form);
        }
    else if (context.request.method === 'POST'){
        var count = context.request.getLineCount('salesorderslists');
            for(var i=0; i< count; i++){
                var check = context.request.getSublistValue('salesorderslists', 'sublist_check', i);                
                if(check != 'F')
                {
                    var entity = context.request.getSublistValue({
                        group: 'salesorderslists',
                        name: 'sublist_customer',
                        line: i
                    });
                    var order = context.request.getSublistValue({
                        group: 'salesorderslists',
                        name: 'sublist_id',
                        line: i
                    });
                    
                    var cust = record.load({
                        type: record.Type.CUSTOMER,
                        id: entity
                    });
                    var emailId = cust.getValue('email');

                    if(emailId){
                        log.debug({title: 'email',details: emailId});
                        email.send({
                            author: 4525,
                            recipients: emailId,
                            subject: 'Payment Reminder',
                            body: 'Please send your payment against order# '+ order});
                    }
                    else {
                        log.debug({title: 'Error', details: 'No email found'});
                    }
                }
                //4525
                
                
            
            }
    }

    }
    return{
        onRequest: onRequest
    }
});